import json

import boto3
import csv
import datetime
import traceback

import logger
import s3_editor

client = boto3.client('s3')



# ==================================================
# 定数
# ==================================================
# トークン名
TOKEN_NAME = 'chunta-kakeibo-token'



# ==================================================
# lambdaで初めに呼び出される関数
# ==================================================
def lambda_handler(event, context):
    try:
        # urlからリクエスト内容を取得
        method = event['params']['path']['method']
        
        
        # ログイン POST /login
        if method == 'login':
            if isLessParams(event['body-json'], ['userId', 'password']):
                return createErrorResponse('parameterError.')
                
            if s3_editor.canLogin(event['body-json']['userId'], event['body-json']['password']):
                token = s3_editor.issueToken(event['body-json']['userId'])
                return createTokenResponse(token)
                
            return createErrorResponse('loginError.')
        
        # ユーザー登録 POST /createUser
        if method == 'createUser':
            if isLessParams(event['body-json'], ['userId', 'password']):
                return createErrorResponse('parameterError.')
            
            msg = s3_editor.createUser(event['body-json']['userId'], event['body-json']['password'])
            
            if 'OK' != msg:
                return createErrorResponse('createUserError. ' + msg)
            
            logger.log('create user : ' + event['body-json']['userId'])
            return createNormalResponse()
        
        # 認可
        if not s3_editor.authorization(event['body-json']['userId'], event['body-json']['token']):
            return createErrorResponse('authorizationError.')
        
        
        # 登録 POST /regist
        if method == 'regist':
            if isLessParams(event['body-json'], ['userId', 'categolly', 'item', 'syusi', 'bikou', 'year', 'month', 'day']):
                return createErrorResponse('parameterError.')
            
            s3_editor.registMeisai(event['body-json']['userId'], event['body-json']['categolly'], event['body-json']['item'], event['body-json']['syusi'], event['body-json']['bikou'], event['body-json']['year'], event['body-json']['month'], event['body-json']['day'])
            
            return createNormalResponse()
        
        # カテゴリー登録 POST /registCategolly
        if method == 'registCategolly':
            if isLessParams(event['body-json'], ['userId', 'categolly']):
                return createErrorResponse('parameterError.')
            
            s3_editor.registCategolly(event['body-json']['userId'], event['body-json']['categolly'])
            
            return createNormalResponse()
        
        # 削除 DELETE /delete
        if method == 'delete':
            if isLessParams(event['body-json'], ['userId', 'meisaiId']):
                return createErrorResponse('parameterError.')
            
            if s3_editor.deleteMeisai(event['body-json']['userId'], event['body-json']['meisaiId']):
                return createNormalResponse()
                
            return createErrorResponse('deleteError.')
        
        # カテゴリー削除 DELETE /deleteCategolly
        if method == 'deleteCategolly':
            if isLessParams(event['body-json'], ['userId', 'categolly']):
                return createErrorResponse('parameterError.')
            
            if s3_editor.deleteCategolly(event['body-json']['userId'], event['body-json']['categolly']):
                return createNormalResponse()
                
            return createErrorResponse('deleteError.')
        
        # 編集 PUT /edit
        if method == 'edit':
            if isLessParams(event['body-json'], ['userId', 'meisaiId', 'categolly', 'item', 'syusi', 'bikou', 'year', 'month', 'day']):
                return createErrorResponse('parameterError.')
            
            s3_editor.editMeisai(event['body-json']['userId'], event['body-json']['meisaiId'], event['body-json']['categolly'], event['body-json']['item'], event['body-json']['syusi'], event['body-json']['bikou'], event['body-json']['year'], event['body-json']['month'], event['body-json']['day'])
            
            return createNormalResponse()
        
        # カテゴリー表示 POST /viewCategolly/userId/
        if method == 'viewCategolly':
            if isLessParams(event['params']['path'], ['userId']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getCategolly(event['params']['path']['userId'])
            
            return createViewResponse(list)
        
        # 全ての家計簿を表示 POST /viewAll/userId
        if method == 'viewAll':
            if isLessParams(event['params']['path'], ['userId']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getAll(event['params']['path']['userId'])
            
            return createViewResponse(list)
        
        # 指定された明細IDの家計簿を表示 POST /viewByCategolly/userId/meisaiId
        # APIGatewayで3番目のパスをyearと定義しているためyearからmeisaiIdを取得する
        if method == 'viewByMeisaiId':
            if isLessParams(event['params']['path'], ['userId', 'year']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getByMeisaiId(event['params']['path']['userId'], event['params']['path']['year'])
            
            return createViewResponse(list)
        
        # 指定されたカテゴリーの家計簿を表示 POST /viewByCategolly/userId/categolly
        # APIGatewayで3番目のパスをyearと定義しているためyearからcategollyを取得する
        if method == 'viewByCategolly':
            if isLessParams(event['params']['path'], ['userId', 'year']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getByCategolly(event['params']['path']['userId'], event['params']['path']['year'])
            
            return createViewResponse(list)
        
        # 指定された月の家計簿を表示 POST /viewByMonth/userId/year/month
        if method == 'viewByMonth':
            if isLessParams(event['params']['path'], ['userId', 'year', 'month']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getByMonth(event['params']['path']['userId'], event['params']['path']['year'], event['params']['path']['month'])
            
            return createViewResponse(list)
        
        # 指定された日付の家計簿を表示 POST /viewByDay/userId/year/month/day
        if method == 'viewByDay':
            if isLessParams(event['params']['path'], ['userId', 'year', 'month', 'day']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getByDay(event['params']['path']['userId'], event['params']['path']['year'], event['params']['path']['month'], event['params']['path']['day'])
            
            return createViewResponse(list)
        
        # 指定された月とカテゴリーの家計簿を表示 POST /viewByCategollyMonth/userId/year/month/day
        if method == 'viewByCategollyMonth':
            if isLessParams(event['params']['path'], ['userId', 'year', 'month', 'day']):
                return createErrorResponse('parameterError.')
            
            list = s3_editor.getByCategollyMonth(event['params']['path']['userId'], event['params']['path']['day'], event['params']['path']['year'], event['params']['path']['month'])
            
            return createViewResponse(list)
        
        
        # 該当する処理が存在しない場合
        return createErrorResponse('notMethodError.')
        
        
    except:
        # 想定外エラー発生時
        logger.log(traceback.format_exc())
        traceback.print_exc()
        
        return createErrorResponse(traceback.format_exc())

# ==================================================
# パラメータが不足しているか判定
# ==================================================
def isLessParams(paramsList, whiteList):
    for param in paramsList:
        for whiteParam in whiteList:
            if whiteParam == param:
                return False
    
    return True

# ==================================================
# 標準レスポンスを生成
# ==================================================
def createNormalResponse():
    return {
            'isBase64Encoded': False,
            'statusCode': 200,
            'headers': {},
            'body': '{}'
        }

# ==================================================
# token発行時のレスポンスを生成
# APIGatewayでHTTPヘッダにもSet-Cookieでtokenを設定
# ==================================================
def createTokenResponse(token):
    return {
            TOKEN_NAME: token
        }

# ==================================================
# エラーレスポンスを生成
# ==================================================
def createErrorResponse(message):
    return {
            'isBase64Encoded': False,
            'statusCode': 400,
            'headers': {},
            'body': '{"message":' + message + '}'
        }

# ==================================================
# 表示用レスポンスを生成
# ==================================================
def createViewResponse(list):
    return {
            'isBase64Encoded': False,
            'statusCode': 200,
            'headers': {},
            'body': list
        }

