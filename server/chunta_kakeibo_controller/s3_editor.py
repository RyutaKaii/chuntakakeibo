import json

import boto3
import botocore
import datetime
import traceback
import hashlib
import random
import string
import urllib.parse

import logger

client = boto3.client('s3')
resource = boto3.resource('s3')



# ==================================================
# 定数
# ==================================================
# トークン名
TOKEN_NAME = 'chunta-kakeibo-token'
# バケット名
BUCKET = 'elasticbeanstalk-us-east-2-561519275591'
# バケットからのパス
PATH_FOLDER = 'chunta_kakeibo/'
# s3ラインセパレーター
LINE_SEPALATER = ','
# s3ファイルセパレーター
FILE_SEPALATER = '\r\n'



# ==================================================
# S3バケットのファイルを操作する機能群
# S3には1つのログイン可能ユーザーリストと、ユーザーごとに下記のファイルが格納されている
# ファイルはすべてjson形式のリスト
# ・'UserList'(1つのログイン可能ユーザーリスト、手動で登録する)
#     userId,password
# ・userId + '_token'
#     token
# ・userId + '_categolly'
#     categolly
# ・userId + '_meisai'
#     meisaiId, categolly, item, syusi, bikou, year, month, day
# ==================================================
# ==================================================
# ログイン可能かを判定
# ==================================================
def canLogin(userId, password):
    list = getJsonObjListFromS3('UserList')
    
    for row in list:
        if row['userId'] == userId and row['password'] == password:
            return True
    
    return False

# ==================================================
# トークンを発行
# 指定されたユーザーに対して有効期限付きトークンを発行する
# ==================================================
def issueToken(userId):
    dat = randomname(10)
    token = hashlib.sha256(dat.encode()).hexdigest()
    
    registList = [ {'token':token} ]
    
    writeJsonObjListToS3(userId + '_token', registList)
    
    return token

# ==================================================
# 利用可能かを判定
# ==================================================
def authorization(userId, token):
    list = getJsonObjListFromS3(userId + '_token')
    
    for row in list:
        if row['token'] == token:
            return True
    
    return False

# ==================================================
# カテゴリーが存在するか判定
# ==================================================
def isExistCategolly(userId, categolly):
    list = getJsonObjListFromS3(userId + '_categolly')
    
    for row in list:
        if row['categolly'] == categolly:
            return True
    
    return False

# ==================================================
# 明細登録
# ==================================================
def registMeisai(userId, categolly, item, syusi, bikou, year, month, day):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    meisaiId = str(len(list) + 1)
    
    list.append(
        {
            'meisaiId':meisaiId,
            'categolly':categolly,
            'item':item,
            'syusi':syusi,
            'bikou':bikou,
            'year':year,
            'month':month,
            'day':convToMToMM(day)
        }
        )
    
    writeJsonObjListToS3(userId + '_meisai', list)

# ==================================================
# カテゴリー登録
# ==================================================
def registCategolly(userId, categolly):
    list = getJsonObjListFromS3(userId + '_categolly')
    
    list.append(
        {
            'categolly':categolly
        }
        )
    
    writeJsonObjListToS3(userId + '_categolly', list)

# ==================================================
# 明細削除
# ==================================================
def deleteMeisai(userId, meisaiId):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    idx = -1;
    deleteIdx = -99
    
    for row in list:
        idx = idx + 1
        
        if row['meisaiId'] == str(meisaiId):
            deleteIdx = idx
    
    if deleteIdx != -99:
        del list[deleteIdx]
        writeJsonObjListToS3(userId + '_meisai', list)
        return True
    
    return False

# ==================================================
# カテゴリー削除
# ==================================================
def deleteCategolly(userId, categolly):
    list = getJsonObjListFromS3(userId + '_categolly')
    
    idx = -1;
    deleteIdx = -99
    
    for row in list:
        idx = idx + 1
        
        if row['categolly'] == str(categolly):
            deleteIdx = idx
    
    if deleteIdx != -99:
        del list[deleteIdx]
        writeJsonObjListToS3(userId + '_categolly', list)
        return True
    
    return False

# ==================================================
# 明細編集
# ==================================================
def editMeisai(userId, meisaiId, categolly, item, syusi, bikou, year, month, day):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    registList = []
    
    for row in list:
        if row['meisaiId'] == meisaiId:
            registrow = {
                'meisaiId':meisaiId,
                'categolly':categolly,
                'item':item,
                'syusi':syusi,
                'bikou':bikou,
                'year':year,
                'month':month,
                'day':convToMToMM(day)
            }
            
            registList.append(registrow)
        else:
            registList.append(row)
        
    writeJsonObjListToS3(userId + '_meisai', registList)

# ==================================================
# カテゴリー取得
# ==================================================
def getCategolly(userId):
    list = getJsonObjListFromS3(userId + '_categolly')
    
    return list

# ==================================================
# 全ての明細を取得
# ==================================================
def getAll(userId):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    return list

# ==================================================
# 指定された明細IDの明細を取得
# ==================================================
def getByMeisaiId(userId, meisaiId):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    resultList = []
    
    for row in list:
        if row['meisaiId'] == meisaiId:
            resultList.append(row)
    
    return resultList

# ==================================================
# 指定されたカテゴリーの明細を取得
# ==================================================
def getByCategolly(userId, categolly):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    categolly = urllib.parse.unquote(categolly)
    
    resultList = []
    
    for row in list:
        if row['categolly'] == categolly:
            resultList.append(row)
    
    return resultList

# ==================================================
# 指定された月の明細を取得
# ==================================================
def getByMonth(userId, year, month):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    resultList = []
    
    for row in list:
        if row['year'] == year and row['month'] == month:
            resultList.append(row)
    
    return resultList

# ==================================================
# 指定された日の明細を取得
# ==================================================
def getByDay(userId, year, month, day):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    resultList = []
    
    for row in list:
        if row['year'] == year and row['month'] == month and row['day'] == convToMToMM(day):
            resultList.append(row)
    
    return resultList

# ==================================================
# 指定された月とカテゴリーの明細を取得
# ==================================================
def getByCategollyMonth(userId, categolly, year, month):
    list = getJsonObjListFromS3(userId + '_meisai')
    
    categolly = urllib.parse.unquote(categolly)
    
    resultList = []
    
    for row in list:
        if row['year'] == year and row['month'] == month:
            resultList.append(row)
    
    resultList2 = []
    
    for row2 in resultList:
        if row2['categolly'] == categolly:
            resultList2.append(row2)
    
    return resultList2

# ==================================================
# 指定されたuserIdとpasswordでユーザーを追加
# ==================================================
def createUser(userId, password):
    # 1日あたりの追加可能ユーザー数を超えている場合はエラー
    # 当分は無制限
    
    # 空文字はエラー
    if userId == '' or password == '':
        return '空文字以外を入力してください。'
    
    # 同名のユーザーが存在する場合はエラー
    list = getJsonObjListFromS3('UserList')
    for row in list:
        if row['userId'] == userId:
            return '同じユーザー名が登録されています。';
    
    # ユーザー毎のファイルを生成
    writeJsonObjListToS3(userId + '_meisai', [])
    writeJsonObjListToS3(userId + '_categolly', [])
    writeJsonObjListToS3(userId + '_token', [])
    
    # ユーザーリストに登録
    list = getJsonObjListFromS3('UserList')
    
    list.append(
        {
            'userId': userId,
            'password': password
        }
    )
    
    writeJsonObjListToS3('UserList', list)
    
    return 'OK';

# ==================================================
# 指定されたファイルからjsonオブジェクトを取得
# ==================================================
def getJsonObjListFromS3(fileName):
    # ファイルが存在しない場合は空を返却
    if not isExistS3File(fileName):
        return []
    
    # s3からファイル読み込み
    s3File = client.get_object(Bucket = BUCKET, Key = PATH_FOLDER + fileName)
    str = s3File['Body'].read().decode('utf-8')
    
    # 空文字の場合は空を返却
    if str == '':
        return []
    
    # json文字列をデコード
    jsonObject = json.loads(str)
    
    return jsonObject

# ==================================================
# 指定されたオブジェクトを書き込む
# ==================================================
def writeJsonObjListToS3(fileName, list):
    writeStr = json.dumps(list)
    
    # s3にアップロード
    s3File = resource.Object(BUCKET, PATH_FOLDER + fileName)
    s3File.put(Body=writeStr)

# ==================================================
# s3上にファイルが存在するか確認
# True 存在する False 存在しない
# ==================================================
def isExistS3File(fileName):
    try:
        client.head_object(Bucket = BUCKET, Key = PATH_FOLDER + fileName)
        return True
    except botocore.exceptions.ClientError:
        return False

# ==================================================
# 指定された桁数のランダム文字列を生成
# ==================================================
def randomname(n):
   randlst = [random.choice(string.ascii_letters + string.digits) for i in range(n)]
   return ''.join(randlst)

# ==================================================
# 日付のM形式をmm形式に変換
# ==================================================
def convToMToMM(day):
   if day == '1' or day == '2' or day == '3' or day == '4' or day == '5' or day == '6' or day == '7' or day == '8' or day == '9':
       return '0' + day

