// ==================================================
// ==================================================
// 定数
// ==================================================
// ==================================================

// テンプレートディレクトリ
var TEMPLATE_DIR = './handlebars/template/';
// ログインページテンプレート名
var TEMPLATE_LOGIN = 'login';
// 登録ページテンプレート名
var TEMPLATE_REGIST = 'regist';
// カテゴリー管理ページテンプレート名
var TEMPLATE_MANAGE_CATEGOLLY = 'manageCategolly';
// 表示ページテンプレート名
var TEMPLATE_VIEW = 'view';
// 編集ページテンプレート名
var TEMPLATE_EDIT = 'edit';
// エラーページテンプレート名
var TEMPLATE_ERROR = 'error';
// ユーザー登録ページテンプレート名
var TEMPLATE_CREATE_USER = 'createUser'



// ==================================================
// ==================================================
// 画面遷移系メソッド
// ==================================================
// ==================================================

// ==================================================
// 初回表示
// ==================================================
window.onload = function() {
	forwardLogin('');
}

// ==================================================
// ログインページへ遷移
// ==================================================
function forwardLogin(data) {
	renderHtml(TEMPLATE_LOGIN, '');
}

// ==================================================
// ユーザー登録ページへ遷移
// ==================================================
function forwardCreateUser() {
	renderHtml(TEMPLATE_CREATE_USER, '');
}

// ==================================================
// 表示ページへ遷移
// ==================================================
function forwardView(categolly, year, month, sort, chk_categolly, chk_yearmonth) {
	// 検索条件を保存
	saveFindItems(categolly, year, month, sort, chk_categolly, chk_yearmonth);
	
	// hiddenに保存されているuserIdを取得
	var userId = $('#hidden_userId').val();
	
	// チェックボックスの値で検索方法を決定しデータを取得
	if ( getChkboxBoolValue(chk_categolly) && getChkboxBoolValue(chk_yearmonth) ) {
		data = ajax_viewByCategollyMonth(userId, categolly, year, month);
	} else if ( getChkboxBoolValue(chk_categolly) ) {
		data = ajax_viewByCategolly(userId, categolly);
	} else if ( getChkboxBoolValue(chk_yearmonth) ) {
		data = ajax_viewByMonth(userId, year, month);
	} else {
		// 何もしない
		return;
	}
	
	// 並び替え
	switch (sort) {
		case "日付が新しい順":
			sortByDate(data);
			break;
		case "日付が古い順":
			sortByDateAsc(data);
			break;
		case "収支が高い順":
			sortBySyusi(data);
			break;
		case "収支が低い順":
			sortBySyusiAsc(data);
			break;
		default:
			// 指定されていない場合は並び替えは実施しない
			break;
	}
	
	// 合計値の文字列を取得
	var sumStr = getSumstrSyusi(data);
	
	// カテゴリー一覧を取得
	var categollyList = ajax_viewCategolly(userId);
	
	var result = {
	    // 表示するデータ
        resultList: data,
        // カテゴリー一覧
        categollyList: categollyList,
        // 合計値
        sumStr: sumStr,
        // 保存する検索条件
        last_chk_categolly: chk_categolly,
        last_chk_yearmonth: chk_yearmonth,
        last_find_categolly: categolly,
        last_find_year: year,
        last_find_month: month,
        last_find_sort: sort
    };
	
	renderHtml(TEMPLATE_VIEW, result);
	
	// 検索条件を設定
	$("#chk_categolly").prop("checked", getChkboxBoolValue(chk_categolly));
	$("#chk_yearmonth").prop("checked", getChkboxBoolValue(chk_yearmonth));
	
	document.getElementById("find_categolly").selectedIndex = -1;
	if (categolly != '') {
		$("#find_categolly").val(categolly);
	}
	document.getElementById("find_year").selectedIndex = -1;
	if (year != '') {
		$("#find_year").val(year);
	}
	document.getElementById("find_month").selectedIndex = -1;
	if (month != '') {
		$("#find_month").val(month);
	}
	document.getElementById("find_sort").selectedIndex = -1;
	if (sort != '') {
		$("#find_sort").val(sort);
	}
}

// ==================================================
// 登録ページへ遷移
// ==================================================
function forwardRegist() {
	// 現在日付文字列を取得
	var year = getNowYearStr();
	var month = getNowMonthStr();
	var day = getNowDayStr();
	
	// カテゴリー一覧を取得
	var userId = $('#hidden_userId').val();
	var categollyList = ajax_viewCategolly(userId);
	
	var data = {
        year: year,
        month: month,
        day: day,
        categollyList: categollyList
    };
    
	renderHtml(TEMPLATE_REGIST, data);
}

// ==================================================
// カテゴリー管理ページへ遷移
// ==================================================
function forwardManageCategolly() {
	var userId = $('#hidden_userId').val();
	
	var result = ajax_viewCategolly(userId);
	
	renderHtml(TEMPLATE_MANAGE_CATEGOLLY, result);
}

// ==================================================
// 編集ページへ遷移
// ==================================================
function forwardEdit(meisaiId) {
	var userId = $('#hidden_userId').val();
	
	var result = ajax_viewByMeisaiId(userId, meisaiId);
	
	renderHtml(TEMPLATE_EDIT, result);
}

// ==================================================
// エラーページへ遷移
// ==================================================
function forwardError() {
	renderHtml(TEMPLATE_ERROR, '');
}



// ==================================================
// ==================================================
// ボタン系メソッド
// ==================================================
// ==================================================

// ==================================================
// ログイン
// ==================================================
function onClickLogin() {
	var userId = $('#userId').val();
	var password = $('#password').val();
	
	if (userId == '' || password == '') {
		alert('ログインIDまたはパスワードが間違っています。');
		forwardLogin('');
		return;
	}
	
	var result = ajax_login(userId, password);
	
	if (result == false) {
		alert('ログインIDまたはパスワードが間違っています。');
		forwardLogin('');
		return;
	}
	
	// ユーザーIDをhiddenに退避
	$('#hidden_userId').val(userId);
	
	// 今月分のデータの日付が新しい順で表示
	forwardView("", getNowYearStr(), getNowMonthStr(), "日付が新しい順", false, true);
}

// ==================================================
// 検索条件変更時の処理
// ==================================================
function onChangeCondition() {
	// プルダウンから検索条件を取得
	var categolly = $('#find_categolly').val();
	var year = $('#find_year').val();
	var month = $('#find_month').val();
	var sort = $("#find_sort").val();
	// チェックボックスからチェック状態を取得
	var chk_categolly = $("#chk_categolly").prop("checked");
	var chk_yearmonth = $("#chk_yearmonth").prop("checked");
	
	// プルダウンで選択している検索条件で表示
	forwardView(categolly, year, month, sort, chk_categolly, chk_yearmonth);
}

// ==================================================
// 登録
// ==================================================
function onClickRegist() {
	var userId = $('#hidden_userId').val();
	var categolly = $('#categolly').val();
	var item = $('#item').val();
	var syusi = $('#syusi').val();
	var bikou = $('#bikou').val();
	var year = $('#year').val();
	var month = $('#month').val();
	var day = $('#day').val();
	
	var result = ajax_regist(userId, categolly, item, syusi, bikou, year, month, day);
	
	if (result == false) {
		forwardError();
		return;
	}
	
	// 前回検索条件のデータで表示
	forwardView($("#last_find_categolly").val(), $("#last_find_year").val(), $("#last_find_month").val(), $("#last_find_sort").val(), $("#last_chk_categolly").val(), $("#last_chk_yearmonth").val());
}

// ==================================================
// 編集
// ==================================================
function onClickEdit() {
	var userId = $('#hidden_userId').val();
	var meisaiId = $('#meisaiId').val();
	var categolly = $('#categolly').val();
	var item = $('#item').val();
	var syusi = $('#syusi').val();
	var bikou = $('#bikou').val();
	var year = $('#year').val();
	var month = $('#month').val();
	var day = $('#day').val();
	
	var result = ajax_edit(userId, meisaiId, categolly, item, syusi, bikou, year, month, day);
	
	if (result == false) {
		forwardError();
		return;
	}
	
	// 前回検索条件のデータで表示
	forwardView($("#last_find_categolly").val(), $("#last_find_year").val(), $("#last_find_month").val(), $("#last_find_sort").val(), $("#last_chk_categolly").val(), $("#last_chk_yearmonth").val());
}

// ==================================================
// 削除
// ==================================================
function onClickDelete(meisaiId) {
	var confirm = window.confirm('削除しますか？');
	
	if (!confirm) {
		return;
	}
	
	var result = ajax_delete($('#hidden_userId').val(), meisaiId);
	
	if (result == false) {
		forwardError();
		return;
	}
	
	// 前回検索条件のデータで表示
	forwardView($("#last_find_categolly").val(), $("#last_find_year").val(), $("#last_find_month").val(), $("#last_find_sort").val(), $("#last_chk_categolly").val(), $("#last_chk_yearmonth").val());
}

// ==================================================
// カテゴリー登録
// ==================================================
function onClickRegistCategolly() {
	var userId = $('#hidden_userId').val();
	var categolly = $('#categolly').val();
	
	var result = ajax_registCategolly(userId, categolly);
	
	if (result == false) {
		forwardError();
		return;
	}
	
	forwardManageCategolly();
}

// ==================================================
// カテゴリー削除
// ==================================================
function onClickDeleteCategolly(categolly) {
	var confirm = window.confirm('削除しますか？');
	
	if (!confirm) {
		return;
	}
	
	var result = ajax_deleteCategolly($('#hidden_userId').val(), categolly);
	
	if (result == false) {
		forwardError();
		return;
	}
	
	forwardManageCategolly();
}

// ==================================================
// ユーザー登録
// ==================================================
function onClickCreateUser() {
	var confirm = window.confirm('登録します。よろしいですか？？');
	
	if (!confirm) {
		return;
	}
	
	var userId = $("#create_userId").val();
	var password = $("#create_password").val();
	
	if (userId == '' || password == '') {
		alert('ユーザーIDおよびパスワードは空文字は使用できません。');
		return;
	}
	
	var result = ajax_createUser( $('#create_userId').val(), $('#create_password').val());
	
	if (result != 'OK') {
		alert(result);
		return;
	}
	
	alert('ユーザー登録が完了しました。');
	forwardLogin('');
}

// ==================================================
// ログアウト
// ==================================================
function onClickLogout() {
	// ユーザーIDとtokenをクリア
	$('#hidden_userId').val('');
	$('#token').val('');
	
	// ログインページへ遷移
	forwardLogin('');
}



// ==================================================
// ==================================================
// Handlebars系メソッド
// ==================================================
// ==================================================

// ==================================================
// htmlを表示
// ==================================================
function renderHtml(name, data) {
	// テンプレート取得
	var template = Handlebars.getTemplate(name);
	
	// データを設定
	var html = template(data);
	
	// 表示
	$("#contents").html(html);
	
	return;
}

// ==================================================
// Handlebarsのテンプレートファイルを読み込む
// ref https://codeday.me/jp/qa/20190403/509032.html
// ==================================================
Handlebars.getTemplate = function(name) {
	if (Handlebars.templates === undefined || Handlebars.templates[name] === undefined) {
		$.ajax({
			url : TEMPLATE_DIR + name + '.txt',
			success : function(data) {
				if (Handlebars.templates === undefined) {
					Handlebars.templates = {};
				}
				Handlebars.templates[name] = Handlebars.compile(data);
			},
			async : false
		});
	}
	
	return Handlebars.templates[name];
};



// ==================================================
// ==================================================
// Util系メソッド
// ==================================================
// ==================================================

// ==================================================
// 本日日付の年の文字列を取得
// ==================================================
function getNowYearStr() {
	var today = new Date();
	
	return String(today.getFullYear());
}

// ==================================================
// 本日日付の月の文字列を取得
// ==================================================
function getNowMonthStr() {
	var today = new Date();
	
	return String(today.getMonth() + 1);
}

// ==================================================
// 本日日付の日の文字列を取得
// ==================================================
function getNowDayStr() {
	var today = new Date();
	
	return String(today.getDate());
}

// ==================================================
// 日付で降順に並び替え
// ==================================================
function sortByDate(data) {
	var tmpData;
	
	for (var i = 0;i < data.length - 1; i++) {
		for (var j = i + 1;j < data.length; j++) {
			var dateStr1 = data[i]['year'] + data[i]['month'] + data[i]['day'];
			var dateStr2 = data[j]['year'] + data[j]['month'] + data[j]['day'];
			
			if (dateStr1 < dateStr2) {
				var tmpData = data[j];
				data[j] = data[i];
				data[i] = tmpData;
			}
		}
	}
}

// ==================================================
// 日付で昇順に並び替え
// ==================================================
function sortByDateAsc(data) {
	var tmpData;
	
	for (var i = 0;i < data.length - 1; i++) {
		for (var j = i + 1;j < data.length; j++) {
			var dateStr1 = data[i]['year'] + data[i]['month'] + data[i]['day'];
			var dateStr2 = data[j]['year'] + data[j]['month'] + data[j]['day'];
			
			if (dateStr1 > dateStr2) {
				var tmpData = data[j];
				data[j] = data[i];
				data[i] = tmpData;
			}
		}
	}
}

// ==================================================
// 収支を降順に並び替え
// ==================================================
function sortBySyusi(data) {
	var intArray = new Array();
	var notintArray = new Array();
	
	// 数字と数字でないものに振り分け
	for (var i = 0;i < data.length; i++) {
		var parsedValue = parseInt(data[i]['syusi']);
		
		if (isNaN(parsedValue)) {
			notintArray.push(data[i]);
			continue;
		}
		
		intArray.push(data[i]);
	}
	
	// 数字を並び替え
	simpleSortBySyusi(intArray, true);
	
	// 数字でないものを並び替え
	simpleSortBySyusi(notintArray, false);
	
	// 数字の配列、数字でないものの配列の順で結合
	var concatArray = intArray.concat(notintArray);
	
	// 並び変えた配列に変更
	for (var j = 0;j < data.length; j++) {
		data[j] = concatArray[j];
	}
}

// ==================================================
// 収支を昇順に並び替え
// ==================================================
function sortBySyusiAsc(data) {
	var intArray = new Array();
	var notintArray = new Array();
	
	// 数字と数字でないものに振り分け
	for (var i = 0;i < data.length; i++) {
		var parsedValue = parseInt(data[i]['syusi']);
		
		if (isNaN(parsedValue)) {
			notintArray.push(data[i]);
			continue;
		}
		
		intArray.push(data[i]);
	}
	
	// 数字を並び替え
	simpleSortBySyusiAsc(intArray, true);
	
	// 数字でないものを並び替え
	simpleSortBySyusiAsc(notintArray, false);
	
	// 数字の配列、数字でないものの配列の順で結合
	var concatArray = intArray.concat(notintArray);
	
	// 並び変えた配列に変更
	for (var j = 0;j < data.length; j++) {
		data[j] = concatArray[j];
	}
}

// ==================================================
// 収支を単純に降順に並び替え
// 第2引数にはデータが数値であるかどうかのフラグを指定
// ==================================================
function simpleSortBySyusi(data, isInt) {
	var tmpData;
	
	for (var i = 0;i < data.length - 1; i++) {
		for (var j = i + 1;j < data.length; j++) {
			var dateStr1 = data[i]['syusi'];
			var dateStr2 = data[j]['syusi'];
			
			if (isInt) {
				dateStr1 = parseInt(data[i]['syusi']);
				dateStr2 = parseInt(data[j]['syusi']);
			}
			
			if (dateStr1 < dateStr2) {
				var tmpData = data[j];
				data[j] = data[i];
				data[i] = tmpData;
			}
		}
	}
}

// ==================================================
// 収支を単純に昇順に並び替え
// 第2引数にはデータが数値であるかどうかのフラグを指定
// ==================================================
function simpleSortBySyusiAsc(data, isInt) {
	var tmpData;
	
	for (var i = 0;i < data.length - 1; i++) {
		for (var j = i + 1;j < data.length; j++) {
			var dateStr1 = data[i]['syusi'];
			var dateStr2 = data[j]['syusi'];
			
			if (isInt) {
				dateStr1 = parseInt(data[i]['syusi']);
				dateStr2 = parseInt(data[j]['syusi']);
			}
			
			if (dateStr1 > dateStr2) {
				var tmpData = data[j];
				data[j] = data[i];
				data[i] = tmpData;
			}
		}
	}
}

// ==================================================
// 検索条件を保存
// ==================================================
function saveFindItems(categolly, year, month, sort, chk_categolly, chk_yearmonth) {
	$("#last_find_categolly").val(categolly);
	$("#last_find_year").val(year);
	$("#last_find_month").val(month);
	$("#last_find_sort").val(sort);
	$("#last_chk_categolly").val(chk_categolly);
	$("#last_chk_yearmonth").val(chk_yearmonth);
}

// ==================================================
// チェックボックスの値を取得
// ==================================================
function getChkboxBoolValue(value) {
	if (null == value || 'undefined' == value || '' == value) {
		return false;
	}
	
	if ('false' == value || false == value) {
		return false;
	}
	
	if ('true' == value || true == value) {
		return true;
	}
	
	return false;
}

// ==================================================
// 収支の合計値を取得
// ==================================================
function getSumstrSyusi(data) {
	var sum = 0;
	var intArray = new Array();
	
	// 数字のみを対象とする
	for (var i = 0;i < data.length; i++) {
		var parsedValue = parseInt(data[i]['syusi']);
		
		if (!isNaN(parsedValue)) {
			sum = sum + parsedValue;
		}
	}
	
	if (sum > 0) {
		return "+" + String(sum);
	} else if (sum == 0) {
		return "±" + String(sum);
	}
	
	return String(sum);
}

