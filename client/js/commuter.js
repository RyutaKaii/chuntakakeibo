// ==================================================
// ==================================================
// 定数
// ==================================================
// ==================================================

// ドメイン
var DOMAIN = 'https://fwgpf04vj8.execute-api.us-east-2.amazonaws.com/dev1';
// トークン名
var TOKEN_NAME = 'chunta-kakeibo-token';



// ==================================================
// ログイン
// true 成功 false 失敗
// ==================================================
function ajax_login(userId, password) {
	var url = '/login';
	
	var data = {
        userId: userId,
        password: password
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		processData: false,
		dataType: 'json',
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var token = result[TOKEN_NAME];
	
	if (null == token || 'undefined' == token || '' == token) {
		return false;
	}
	
	$('#token').val(token);
	return true;
}

// ==================================================
// 登録
// true 成功 false 失敗
// ==================================================
function ajax_regist(userId, categolly, item, syusi, bikou, year, month, day) {
	var url = '/regist';
	
	var data = {
        userId: userId,
		categolly: categolly,
		item: item,
		syusi: syusi,
		bikou: bikou,
		year: year,
		month: month,
		day: day,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return false;
	}
	
	return true;
}

// ==================================================
// カテゴリー登録
// true 成功 false 失敗
// ==================================================
function ajax_registCategolly(userId, categolly) {
	var url = '/registCategolly';
	
	var data = {
        userId: userId,
		categolly: categolly,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return false;
	}
	
	return true;
}

// ==================================================
// 削除
// true 成功 false 失敗
// ==================================================
function ajax_delete(userId, meisaiId) {
	var url = '/delete';
	
	var data = {
        userId: userId,
		meisaiId: meisaiId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'DELETE',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return false;
	}
	
	return true;
}

// ==================================================
// カテゴリー削除
// true 成功 false 失敗
// ==================================================
function ajax_deleteCategolly(userId, categolly) {
	var url = '/deleteCategolly';
	
	var data = {
        userId: userId,
		categolly: categolly,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'DELETE',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return false;
	}
	
	return true;
}

// ==================================================
// 編集
// true 成功 false 失敗
// ==================================================
function ajax_edit(userId, meisaiId, categolly, item, syusi, bikou, year, month, day) {
	var url = '/edit';
	
	var data = {
        userId: userId,
		meisaiId: meisaiId,
		categolly: categolly,
		item: item,
		syusi: syusi,
		bikou: bikou,
		year: year,
		month: month,
		day: day,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'PUT',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return false;
	}
	
	return true;
}

// ==================================================
// 表示用データを取得 全ての家計簿を表示
// ==================================================
function ajax_viewAll(userId) {
	var url = '/viewAll/' + userId;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'];
}

// ==================================================
// 表示用データを取得 指定されたカテゴリーの家計簿を表示
// ==================================================
function ajax_viewByCategolly(userId, categolly) {
	var url = '/viewByCategolly/' + userId + '/' + categolly;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'];
}

// ==================================================
// 表示用データを取得 指定された月の家計簿を表示
// ==================================================
function ajax_viewByMonth(userId, year, month) {
	var url = '/viewByMonth/' + userId + '/' + year + '/' + month;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'];
}

// ==================================================
// 表示用データを取得 指定された日付の家計簿を表示
// ==================================================
function ajax_viewByDay(userId, year, month, day) {
	var url = '/viewByDay/' + userId + '/' + year + '/' + month + '/' + day;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'];
}

// ==================================================
// 表示用データを取得 指定されたカテゴリーと月の家計簿を表示
// ==================================================
function ajax_viewByCategollyMonth(userId, categolly, year, month) {
	var url = '/viewByCategollyMonth/' + userId + '/' + year + '/' + month + '/' + categolly;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'];
}

// ==================================================
// 編集用データを取得 指定されたmeisaiIdの家計簿を表示
// ==================================================
function ajax_viewByMeisaiId(userId, meisaiId) {
	var url = '/viewByMeisaiId/' + userId + '/' + meisaiId;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'][0];
}

// ==================================================
// 表示用データを取得 カテゴリー表示
// ==================================================
function ajax_viewCategolly(userId) {
	var url = '/viewCategolly/' + userId;
	
	var data = {
		userId: userId,
		token: $('#token').val()
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		dataType: 'json',
		processData: false,
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		return [];
	}
	
	return result['body'];
}

// ==================================================
// ユーザー登録
// true 成功 false 失敗
// ==================================================
function ajax_createUser(userId, password) {
	var url = '/createUser';
	
	var data = {
        userId: userId,
        password: password
    };
	
	var result = $.ajax({
		url: DOMAIN + url,
		type: 'POST',
		contentType: 'application/json',
		processData: false,
		dataType: 'json',
		data:JSON.stringify(data),
		async: false
	}).responseJSON;
	
	var statusCode = result['statusCode'];
	
	if (200 != statusCode) {
		// 想定外エラー
		if (null == result['body']['message'] || 'undefined' == result['body']['message'] || '' == result['body']['message']) {
			return 'ユーザー登録に失敗しました。\n時間をおいて再度お試しください。';
		}
		
		// 準正常
		return 'ユーザー登録に失敗しました。\n' + result['body']['message'];
	}
	
	return 'OK';
}

